# The Lab RFC
Replacement for the build system, package manager and other related changes

- Name: Can be changed if a better name is suggested
- Start date: 29.02.2024


# Status
This RFC is **Work In Progress**

# Summary / Abstract
This is less about making changes and more about discussing them (for now). 
I wanted to talk about this before doing anything.


[TOC]

# Body
## Introduction

This is less about making changes and more about discussing them (for now). 
The Lab was chosen because of similarities of real labs to this one.

- Doing chemistry --> building packages
- Running experiments --> running test
- Installing equipment --> installing packages

I would like to agree on High Level API of the library and CLI commands before any work is started. 
If you have any other ideas, then let's talk about them.

## Configuration with single file
Single file configuration can be useful if you want to move to different hardware, or deploy a system on many servers. 
If it doesn't hurt user experience, then it should be used. 

Also, it would be nice if you didn't need root to modify user part of the config.

## Easy of use (NixOS I am looking at you)
Any changes shouldn't get in the way of users or developers. 
For example, changing the way to install packages. 
CLI has been used for decades to install packages, changing to something like edit a config file then run a command would get in the way of users. 
If we use a single file for configuration and installed packages, then it should be changeable with a command.

## Config format
I think TOML is ok. But it would be nice to have a better syntax for if statements. 
Maybe mixing it with rhai or Lua would make it better.

## Project structure

I think we can have `cli`, `lib`, `gui` folder and use a workspace to manage them.

Having a separate library that does the low level stuff can simplify integration for other developers. 
And will make it easier to develop a GUI.


Here is an example dependency graph for this:
```mermaid
graph TD;
  cli ---> lib;
  cli --> clap;
  
  gui ---> lib;
  gui --> slint;

  3rd-party ---> lib;

  lib --> serde;

  lib --> git2;
  lib --> xxhash-rust/blake3;
  lib --> orz/zstd;
  lib --> nix/libredox;
```

### Implementation and Transition
The change described here are large and won't be implemented for a long time.
The purpose is to know the maximum size, so that The Lab is structured and developed appropriately.

The Lab will be worked on until it is in a good state and can replace existing infrastructure.

### API
The library needs to go back and forth to fix conflicts or report status.
I suggest that we have a Callback trait and when creating an instance it has to be provided.

Example:
```rust
pub trait Callback {
    fn start_download(&mut self, length: u64, file: &str);
    fn increment_downloaded(&mut self, downloaded: usize);
    fn end_download(&mut self);

    fn conflict(&mut self, conflict: Conflict) -> Resolution;

    // not recoverable
    fn error(&mut self, error: Error);
}

let library = lab-lib::Library::new(&mut MyCallback);
```

The CLI should also be extendable (like cargo or git). 
This allows for developers to write custom commands to simplify their experience.
For example `lab build image qemu` will be used to instead of `make rebuild image qemu`. 
Some extension like `lab todo`(print all todos in a project) should also work globally.

## Build system
Use something like Cargo.lock for dependencies. 
Having version in the main file can be annoying to update and doesn't work with version tags.

### Reproducible packages
Everything the package depends on has to be declared in the recipe (including compiler). 
This way in case or regression programs like dash don't break.

### Testing (and fuzzing)
It is important to find bugs before they are released. When designing, this should be a priority.

[OSS Fuzz](https://github.com/google/oss-fuzz) is an example of how automated fuzzing is done. 
For rust specifically, see [Fuzz Book](https://rust-fuzz.github.io/book/). 
For kernel fuzzing I found a [blog post](https://hackmag.com/security/linux-fuzzing/), 
it isn't super interesting since a lot of components of redox are in userspace.

### Automation
Automated fuzzing and building of new version is a nice thing to have in the future. 
I think having a cron job to do that is good enough.

### Version tag
`rust-patched@latest`, `kernel@git`, `ion@stable`

This is similar to docker. By default, it is going to be `git` for testing, but for releases `latest` should be used.

Stable can be delayed latest (or something else). Custom tags should be allowed.

### Feature flags
feature flags can be useful sometimes. Cargo has a nice implementation, 
but I would argue that Gentoo USE flags are a bit better, since you can disable default features with a minus (e.g. `-gnome`, `-*`, `kde`)



## Package management
### Version management
We can have something like `/nix/store` where all packages are extracted. 
To use them, we can have a link to a wrapper that will make env variables suitable for the command being run. Wrapper is used because different app will need different versions of a dependency.

/bin/git links to wrapper. Wrapper reads that 0th argument is git and starts git.

Maybe this should be optional.

### Package format
pkgar is ok, but it depends on a [deprecated crate](https://github.com/sodiumoxide/sodiumoxide). I think a compressed tar with signing is a bit better (the way arch does it)

### Rollback
Same as NixOS generations. Rolling back can be very useful if something breaks.

### Isolation
This can be optional since some packages can't be isolated, like kernel. Having a permission system like flatpak will be very useful. 
It is also a lot safer than giving all permissions.

### Source vs Binary
Why not both? (like NixOS)

We can have a cache for all latest packages. Average users won't need to compile anything, since everything they need is in the cache. 
If something isn't in the cache, then it will be build. 

Cache will only have default features.


# Motivation

Why are we doing this?

Better experience for developers and users.


# RFC Structure

I choose a different structure because It is more about discussing then making changes

Useful link for me:
https://gitlab.redox-os.org/redox-os/rfcs/-/blob/master/0000-template.md
https://www.rfc-editor.org/materials/tutorial76.pdf#page=62
https://docs.gitlab.com/ee/user/markdown.html
